import { useQuery } from 'react-query';
import { api } from '../api';

export const useForecast = () => {
    const { data, isFetched } = useQuery('weather', api.getWeather);

    return (
        {
            data: Array.isArray(data)
                ? data?.slice(0, 7)
                : [],
            isFetched,
        }
    );
};
