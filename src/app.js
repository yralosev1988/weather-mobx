import { useContext, useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { CurrentWeather } from './components/CurrentWeather';
import { Filter } from './components/Filter';
import { Forecast } from './components/Forecast';
import { Header } from './components/Header';
import { useForecast, useStore } from './hooks';
import { Context } from './lib/Provider';

export const App = observer(() => {
    const { data, isFetched } = useForecast();
    const { selectedDayId, setSelectedDayId } = useContext(Context);
    const { filteredDays, isFiltered } = useStore();

    const dataUPD = isFiltered ? filteredDays(data) : data;
    const currentForecast = dataUPD?.filter((item) => item.id === selectedDayId)[ 0 ];

    useEffect(() => {
        if (isFetched) {
            setSelectedDayId(dataUPD[ 0 ]?.id);
        }
    }, [isFetched, dataUPD[ 0 ]?.id]);

    return (
        <main>
            <Filter />
            { currentForecast
                && <>
                    <Header
                        type = { currentForecast?.type }
                        day = { currentForecast?.day } />
                    <CurrentWeather
                        temperature = { currentForecast?.temperature }
                        type = { currentForecast?.type }
                        humidity = { currentForecast?.humidity }
                        rainProbability = { currentForecast?.rain_probability } />
                </>

            }
            <Forecast data = { dataUPD } />
        </main>
    );
});

