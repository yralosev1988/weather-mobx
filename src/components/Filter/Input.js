export const Input = (props) => {
    return (
        <p className = 'custom-input'>
            <label htmlFor = { props.id }>{ props.label }</label>
            <input
                id = { props.id }
                type = { props.type }
                value = { props.value }
                { ...props.register }
                { ...props } />
        </p>
    );
};

Input.defaultProps = {
    type: 'text',
};
