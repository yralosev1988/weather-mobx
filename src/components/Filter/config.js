import * as yup from 'yup';

export const schema = yup.object().shape({
    minTemperature: yup
        .number()
        .transform((cv, ov) => {
            return ov === '' ? undefined : cv;
        }),
    maxTemperature: yup
        .number()
        .transform((cv, ov) => {
            return ov === '' ? undefined : cv;
        }),
});
