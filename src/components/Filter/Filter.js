import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import { schema } from './config';
import { Context } from '../../lib/Provider';
import { Input } from './Input';

export const Filter = observer(() => {
    const { register, handleSubmit, reset } = useForm({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    const {
        type,
        setType,
        isFormBlocked,
        setMaxTemperature,
        setMinTemperature,
        isFiltered,
        applyFilter,
        resetFilter,
        minTemperature,
        maxTemperature,
    } = useContext(Context);

    const onSubmit = (data) => {
        applyFilter(data);
    };

    const resetFilterHandler = () => {
        reset();
        resetFilter();
    };

    return (
        <form className = 'filter'>
            <Input
                id = 'cloudy'
                label = 'Облачно'
                type = 'radio'
                value = { type }
                disabled = { isFiltered }
                className = { `checkbox ${type === 'cloudy' ? 'selected' : ''} ${isFiltered ? 'blocked' : ''}` }
                onChange = { () => setType('cloudy') }
                register = { register('type') } />
            <Input
                id = 'sunny'
                label = 'Солнечно'
                type = 'radio'
                value = { type }
                disabled = { isFiltered }
                className = { `checkbox ${type === 'sunny' ? 'selected' : ''} ${isFiltered ? 'blocked' : ''}` }
                onChange = { () => setType('sunny') }
                register = { register('type') } />
            <Input
                id = 'min-temperature'
                label = 'Минимальная температура'
                type = 'number'
                value = { minTemperature }
                disabled = { isFiltered }
                onChange = { (customEv) => setMinTemperature(customEv.target.value) }
                register = { register('minTemperature') } />
            <Input
                id = 'max-temperature'
                label = 'Максимальная температура'
                type = 'number'
                value = { maxTemperature }
                disabled = { isFiltered }
                onChange = { (customEv) => setMaxTemperature(customEv.target.value) }
                register = { register('maxTemperature') } />
            <button
                type = 'submit'
                disabled = { isFormBlocked }
                onClick = { isFiltered
                    ? resetFilterHandler
                    : handleSubmit(onSubmit) }>
                { isFiltered ? 'Сбросить' : 'Фильтровать' }
            </button>
        </form>
    );
});
