import { format } from 'date-fns';
import { ru } from 'date-fns/locale';

export const Header = (props) => {
    const { day, type } = props;

    return (
        <div className = 'head'>
            <div className = { `icon ${type}` } />
            <div className = 'current-date'>
                <p>{ format(new Date(day), 'eeee', { locale: ru }) }</p>
                <span>{ format(new Date(day), 'dd MMMM', { locale: ru }) }</span>
            </div>
        </div>
    );
};
