export const CurrentWeather = (props) => {
    const {
        temperature, humidity, rainProbability,
    } = props;

    return (
        <div className = 'current-weather'>
            <p className = 'temperature'>{ temperature }</p>
            <p className = 'meta'>
                <span className = 'rainy'>%{ rainProbability }</span>
                <span className = 'humidity'>%{ humidity }</span>
            </p>
        </div>
    );
};
