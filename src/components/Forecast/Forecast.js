import { observer } from 'mobx-react-lite';
import { ForecastSingleDay } from '../ForecastSingleDay';
import { useStore } from '../../hooks';

export const Forecast = observer((props) => {
    const { data } = props;
    const { isFiltered } = useStore();

    const forecastJSX = data
        .map(
            (item) => (
                <ForecastSingleDay
                    key = { item.id }
                    id = { item.id }
                    type = { item.type }
                    day = { item.day }
                    temperature = { item.temperature } />),
        );

    return (
        <div className = 'forecast'>
            {
                !data.length && isFiltered
                    ? <p className = 'message'>По заданным критериям нет доступных дней!</p>
                    : forecastJSX
            }
        </div>
    );
});
