import { format } from 'date-fns';
import { observer } from 'mobx-react-lite';
import { useContext } from 'react';
import { ru } from 'date-fns/locale';
import { Context } from '../../lib/Provider';

export const ForecastSingleDay = observer((props) => {
    const {
        type, id, day, temperature,
    } = props;

    const { selectedDayId, setSelectedDayId } = useContext(Context);

    return (
        <div
            onClick = { () => setSelectedDayId(id) }
            className = { `day ${type} ${selectedDayId === id ? 'selected' : ''}` }>
            <p>{ format(new Date(day), 'eeee', { locale: ru }) }</p>
            <span>{ temperature }</span>
        </div>
    );
});
